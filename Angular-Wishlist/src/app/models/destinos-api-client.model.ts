import {destinoViaje} from './destino-Viaje.model';
import {Subject, BehaviorSubject } from 'rxjs';

export class DestinosApiClient{
    destinos: destinoViaje[];
    current: Subject<destinoViaje> = new BehaviorSubject<destinoViaje>(null);
    constructor(){
        this.destinos = [];
    }

    add(d: destinoViaje){
        this.destinos.push(d)
    }

    getAll(): destinoViaje[]{
        return this.destinos;
    }

    getById(id: String): destinoViaje{
        return this.destinos.filter(function(d) {return d.isSelected.toString() === id;}) [0];
    }

    elegir(d: destinoViaje){
        this.destinos.forEach(x => x.setSelect(false));
        d.setSelect(true);
        this.current.next(d);
    }

    subSuscribeOnChange(fn){
        this.current.subscribe(fn);
    }
}