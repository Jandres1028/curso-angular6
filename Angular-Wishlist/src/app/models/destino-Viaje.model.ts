export class destinoViaje{
    private selected: boolean;
    public servicios: string[];

    constructor( public nombre: string,public u: string){ 
        this.servicios = ['pileta', 'desayuno'];
    }
    
    isSelected(): boolean{
        return this.selected;
    }

    setSelect(s: boolean){
        this.selected = s;
    }
}