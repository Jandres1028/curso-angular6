import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { destinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient} from './../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<destinoViaje>;
  updates: string[];
 
  destinos: destinoViaje[];

  constructor() { 
    this.destinos = [];
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinosApiClient.subscribeOnChange((d: destinoViaje)=>{
        if(d != null){
          this.updates.push('Se ha elegido a ' + d.nombre);
        }
    });
  }

  ngOnInit(): void { }

  guardar(nombre: string, url:string){
    this.destinos.push(new destinoViaje(nombre, url));
    console.log(this.destinos);
    return false;
  }

  agregado(d: destinoViaje){
    //this.destinos.push(d);
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: destinoViaje){
    this.destinosApiClient.elegir(e);
  }

}
