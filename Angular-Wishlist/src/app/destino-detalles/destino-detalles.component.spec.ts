import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoDetallesComponent } from './destino-detalles.component';

describe('DestinoDetallesComponent', () => {
  let component: DestinoDetallesComponent;
  let fixture: ComponentFixture<DestinoDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinoDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
